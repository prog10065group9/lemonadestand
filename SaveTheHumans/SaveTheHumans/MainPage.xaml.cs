﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Animation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SaveTheHumans
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //random object created used to acess the "Next" property
        Random _rand = new Random();

        //object used to track the time of the enemies
        DispatcherTimer enemyTimer = new DispatcherTimer();
        DispatcherTimer targetTimer = new DispatcherTimer();
        bool _humanCaptured = false;
        public MainPage()
        {
            this.InitializeComponent();
            enemyTimer.Tick += enemyTimer_Tick;
            enemyTimer.Interval = TimeSpan.FromSeconds(2);

            targetTimer.Tick += targetTimer_Tick;
            targetTimer.Interval = TimeSpan.FromSeconds(.1);

        }

        private void targetTimer_Tick(object sender, object e)
        {
            _uiProgressBar.Value += 1;
            if (_uiProgressBar.Value >= _uiProgressBar.Maximum)
            {
                EndTheGame();
            }
        }

        private void EndTheGame()
        {
            if (!_uiPlayArea.Children.Contains(_txtGameOverText))
            {
                enemyTimer.Stop();
                targetTimer.Stop();
                _humanCaptured = false;
                _btnStart.Visibility = Visibility.Visible;
                _uiPlayArea.Children.Add(_txtGameOverText);
            }
        }

        private void enemyTimer_Tick(object sender, object e)
        {
            AddEnemy();
        }

        private void start_ButtonClick(object sender, RoutedEventArgs e)
        {
            StartGame();
        }

        private void StartGame()
        {
            human.IsHitTestVisible = true;
            _humanCaptured = false;
            _uiProgressBar.Value = 0;
            _btnStart.Visibility = Visibility.Collapsed;
            _uiPlayArea.Children.Clear();
            _uiPlayArea.Children.Add(human);
            enemyTimer.Start();
            targetTimer.Start();
        }

        private void AddEnemy()
        {
            ContentControl enemy = new ContentControl();
            enemy.Template = Resources["EnemyTemplate"] as ControlTemplate;
            AnimateEnemy(enemy, 0, _uiPlayArea.ActualWidth - 100, "(Canvas.Left)");
            AnimateEnemy(enemy, _rand.Next((int)_uiPlayArea.ActualHeight - 100), _rand.Next((int)_uiPlayArea.ActualHeight - 100), "(Canvas.Top)");
            _uiPlayArea.Children.Add(enemy);

            enemy.PointerEntered += Enemy_PointerEntered;

        }

        private void Enemy_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            if (_humanCaptured)
            {
                EndTheGame();
            }
        }

        private void AnimateEnemy(ContentControl enemy, double from, double to, string propertyToAnimate)
        {
            Storyboard storyboard = new Storyboard() { AutoReverse = true, RepeatBehavior = RepeatBehavior.Forever };
            DoubleAnimation animation = new DoubleAnimation()
            {
                From = from,
                To = to,
                Duration = new Duration(TimeSpan.FromSeconds(_rand.Next(4, 6)))
            };

            Storyboard.SetTarget(animation, enemy);
            Storyboard.SetTargetProperty(animation, propertyToAnimate);
            storyboard.Children.Add(animation);
            storyboard.Begin();
        }

        private void human_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            if (enemyTimer.IsEnabled)
            {
                _humanCaptured = true;
                human.IsHitTestVisible = false;
            }
        }

        private void _uiPlayArea_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            Point pointerPosition = e.GetCurrentPoint(null).Position;
            Point relativePosition = grid.TransformToVisual(_uiPlayArea).TransformPoint(pointerPosition);
            if ((Math.Abs(relativePosition.X - Canvas.GetLeft(human)) > human.ActualWidth * 3) || (Math.Abs(relativePosition.Y - Canvas.GetTop(human)) > human.ActualHeight * 3))
            {
                _humanCaptured = false;
                human.IsHitTestVisible = true;
            }

            else
            {
                Canvas.SetLeft(human, relativePosition.X - human.ActualWidth / 2);
                Canvas.SetTop(human, relativePosition.Y - human.ActualHeight / 2);
            }
        }

        private void _uiPlayArea_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            if(_humanCaptured)
            {
                EndTheGame();
            }
        }

        private void _uiTarget_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            if (targetTimer.IsEnabled && _humanCaptured)
            {
                _uiProgressBar.Value = 0;
                Canvas.SetLeft(_uiTarget, _rand.Next(100, (int)_uiPlayArea.ActualWidth - 100));
                Canvas.SetTop(_uiTarget, _rand.Next(100, (int)_uiPlayArea.ActualHeight - 100));
                Canvas.SetLeft(human, _rand.Next(100, (int)_uiPlayArea.ActualWidth - 100));
                Canvas.SetLeft(human, _rand.Next(100, (int)_uiPlayArea.ActualHeight - 100));
                _humanCaptured = false;
                human.IsHitTestVisible = true;
            }
        }
    }
}
