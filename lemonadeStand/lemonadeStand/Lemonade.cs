﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lemonadeStand
{
    class Lemonade
    {
        /// <summary>
        /// Field to access ice quantity
        /// </summary>
        private int _iceInLemonade;

        /// <summary>
        /// Field to access sugar quantity
        /// </summary>
        private int _sugarInLemonade;

        /// <summary>
        /// Field to access sell price
        /// </summary>
        private float _sellPrice;

        /// <summary>
        /// read-only property
        /// to return amount of ice
        /// in lemonade
        /// </summary>
        public int IceInLemonade
        {
            get { return _iceInLemonade; }
        }

        /// <summary>
        /// read-only property
        /// to return amount of sugar
        /// in lemonade
        /// </summary>
        public int SugarInLemonade
        {
            get { return _sugarInLemonade; }
        }

        /// <summary>
        /// read-only property
        /// to return sell price
        /// of lemonade
        /// </summary>
        public float SellPrice
        {
            get { return _sellPrice; }
        }
    }
}
