﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace lemonadeStand
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LemonadePage : Page
    {
        /// <summary>
        /// Field of type inventory to
        /// access the inventory
        /// </summary>
        public Inventory _inventory;
        public Shop _shop;

        public LemonadePage()
        {
            this.InitializeComponent();
            //create new inventory object
            _inventory = new Inventory();

            //display quantities from inventory object in text blocks
            _txtIceQuantity.Text = _inventory.IceQuantity.ToString();
            _txtLemonQauntity.Text = _inventory.LemonsQuantity.ToString();
            _txtSugarQuantity.Text = _inventory.SugarQuantity.ToString();
            _txtSugarQuantity.Text = _inventory.CupsQuantity.ToString();

            _shop = new Shop();
        }
    }
}
