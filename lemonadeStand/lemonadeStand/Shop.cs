﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lemonadeStand
{
    
    public class Shop
    {
        /// <summary>
        /// Constants that store the price of each item that will be used
        /// by the shopPage to display prices on the screen
        /// </summary>
        private const double CUP_PRICE = 1.00;

        private const double SUGAR_PRICE = 3.00;

        private const double LEMON_PRICE = 3.0;

        private const double ICE_PRICE = 1.25;

        /// <summary>
        /// Read only property that returns Cup price
        /// </summary>
        public double CupPrice
        {
            get
            {
                return CUP_PRICE;
            }
        }

        /// <summary>
        /// Read only property that returns Sugar price
        /// </summary>
        public double SugarPrice
        {
            get
            {
                return SUGAR_PRICE;
            }
        }

        /// <summary>
        /// Read only property that returns Lemon price
        /// </summary>
        public double Lemon_Price
        {
            get
            {
                return LEMON_PRICE;
            }
        }

        /// <summary>
        /// Read only property that returns Ice price
        /// </summary>
        public double Ice_Price
        {
            get
            {
                return ICE_PRICE;
            }
        }
    }
}
