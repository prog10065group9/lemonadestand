﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lemonadeStand
{
    /// <summary>
    /// Decides the preference of the customers per instance of the game, hope I don't mess this up, should only be loaded once per instance of the game.
    /// </summary>
    class Preference
    {
        private Random randomizer;
        private bool _notGenerated;
        /// <summary>
        /// Customer's preferred amount of ice per pitcher
        /// </summary>
        private int _prefIce;
        /// <summary>
        /// Customer's preferred amount of lemons per pitcher
        /// </summary>
        private int _prefLemon;
        /// <summary>
        /// Customer's preferred amount of sugar per pitcher
        /// </summary>
        private int _prefSugar;
        /// <summary>
        /// Customer's preferred cost per cup
        /// </summary>
        private int _prefCost;
        public int PrefIce
        {
            get { return _prefIce; }
        }
        public int PrefLemon
        {
            get { return _prefLemon; }
        }
        public int PrefSugar
        {
            get { return _prefSugar; }
        }
        public bool Generated
        {
            get { return _notGenerated; }
        }
        /// <summary>
        /// Minimum value for the preferred ingredient amount
        /// </summary>
        private const int PREF_INGREDIENT_MIN = 0;
        /// <summary>
        /// Maximum value for the preferred ingredient amount
        /// </summary>
        private const int PREF_INGREDIENT_MAX = 5;
        public Preference()
        {
            randomizer = new Random();
            if (_notGenerated == true)
            {
                GenPrefIngredient(_prefIce);
                GenPrefIngredient(_prefLemon);
                GenPrefIngredient(_prefSugar);
            }
        }

        private void GenPrefIngredient(int ingredient)
        {
            ingredient = randomizer.Next(PREF_INGREDIENT_MIN, PREF_INGREDIENT_MAX);
        }
    }
}
