﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lemonadeStand
{
    /// <summary>
    /// Represents the weather conditions
    /// </summary>
    enum WeatherConditions
    {
        Cloudy = 0,
        Clear,
        Hot
    }
    class DailyWeather
    {
        /// <summary>
        /// randomizer used to extract random weather conditions from the enum
        /// </summary>
        private Random _randomizer;

        /// <summary>
        /// Constructor used to initialize the randomizer
        /// </summary>
        public DailyWeather()
        {
            _randomizer = new Random();
        }

        /// <summary>
        /// Returns a randomly generated weather condition using the randomizer and 
        /// the WeatherCondition Enum
        /// </summary>
        public WeatherConditions GenerateWeather()
        {
            //Store a random value from 0-2 inside the local variable
            //explicitly type casted the randomizer for a appropriate return value 
            WeatherConditions currentWeather = (WeatherConditions)_randomizer.Next(0,3);

            //check random value stored in the currentWeather variable and 
            //return the corresponding enumarated value
            if (currentWeather == (WeatherConditions) 0 )
            {
                return WeatherConditions.Cloudy;
            }
            else if(currentWeather == (WeatherConditions) 1)
            {
                return WeatherConditions.Clear;
            }
            else
            {
                return WeatherConditions.Hot;
            }



        }
    }
}
