﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace lemonadeStand
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ShopPage : Page
    {
        /// <summary>
        /// The shop object that will be used to acess the prices of each item
        /// </summary>
        public Shop _shop;
        
        /// <summary>
        /// The inventory object that will be used to acess the avalaible balance property
        /// </summary>
        public Inventory _inventory;

        /// <summary>
        /// As soon as the shop page is loaded the prices of each item and the avalible balance will be initialized accordingly and
        /// will be displayed to the user
        /// The weather will also be initialized and displayed to the user at random with the help of the DailyWeather class
        /// </summary>
        public ShopPage()
        {
            this.InitializeComponent();
            //a new shop object is craeted every time the page is loaded
             _shop = new Shop();
            
            //a new Inventory object is craeted every time the page is loaded
            _inventory = new Inventory();

            //DailyWeather object that is created everytime the page is loaded
            DailyWeather _weather = new DailyWeather();

            //Displays value of the cups on the screen using the values from the shop class
            _txtCupPrice.Text = "$" + _shop.CupPrice.ToString();

            //Displays value of the sugar on the screen using the values from the shop class
            _txtSugarPrice.Text = "$" + _shop.SugarPrice.ToString();

            //Displays value of the lemons on the screen using the values from the shop class
            _txtLemonPrice.Text = "$" + _shop.Lemon_Price.ToString();

            //Displays value of the ice on the screen using the values from the shop class
            _txtIcePrice.Text = "$" + _shop.Ice_Price.ToString();
            
            //Displays the starting balance on the screen using the property from the inventory class
            _txtPlayerMoney.Text = "$" + _inventory.AvailableBalance.ToString();

            //Displays the Weather at the top of the shop page using the GenerateWeather method 
            //from the DailyWeather class
            _txtWeather.Text = "Looks like the weather will be " + _weather.GenerateWeather().ToString() + " today!";
        }

        /// <summary>
        /// Navigates to LemonadePage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnNavigateToLemonade(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(LemonadePage));
        }
    }
}
