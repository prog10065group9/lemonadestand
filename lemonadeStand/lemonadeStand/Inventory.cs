﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lemonadeStand
{
    public class Inventory
    {
        /// <summary>
        /// Field to access ice
        /// </summary>
        private int _iceQuantity;

        /// <summary>
        /// Field to access lemons
        /// </summary>
        private int _lemonsQuantity;

        /// <summary>
        /// Field to access sugar
        /// </summary>
        private int _sugarQuantity;

        /// <summary>
        /// Field to access money
        /// </summary>
        private float _money;

        /// <summary>
        /// Field to access cups quantity
        /// </summary>
        private int _cupsQuantity;

        /// <summary>
        /// Constant to access available balance
        /// </summary>
        private const double AVAILABLE_BALANCE = 50.00;
 
        /// <summary>
        /// Read-only property to 
        /// return ice quantity
        /// </summary>
        public int IceQuantity
        {
            get { return _iceQuantity; }
        }

        /// <summary>
        /// Read-only property to 
        /// return lemons quantity
        /// </summary>
        public int LemonsQuantity
        {
            get { return _lemonsQuantity; }
        }

        /// <summary>
        /// Read-only property to 
        /// return sugar quantity
        /// </summary>
        public int SugarQuantity
        {
            get { return _sugarQuantity; }
        }

        /// <summary>
        /// Read-only property to 
        /// return money 
        /// </summary>
        public float Money
        {
            get { return _money; }
        }

        /// <summary>
        /// Read-only property to 
        /// return cups quantity
        /// </summary>
        public int CupsQuantity
        {
            get { return _cupsQuantity; }
        }

        /// <summary>
        /// Read-only property to 
        /// return available balance 
        /// </summary>
        public double AvailableBalance
        {
            get { return AVAILABLE_BALANCE; }
        }
    }
}
